import Vue from 'vue'
import App from './App.vue'
import router from './router'
import NavBar from './components/NavBar.vue'

Vue.config.productionTip = false
Vue.component('nav-bar', NavBar)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
